# Analyse database _classicmodels_

## Modification du modèle.

1. Faire une version 2 du modèle de donnée. 

__On veut apporter les changements suivants:__

   1. Utiliser classicmodel.customer.country pour stocker la clef du pays venant de la table world.country
   2. Idem pour classicmodel.offices.country.
   3. Remplacer le type du champ classicmodel.orders.status par un enum de tous les états différents dans la base de données.
   4. Ajouter un champ classicmodel.customer.language qui devra contenir le code de la langue avec laquelle s’adresser au client en ISO 639-1.

2. Écrire le code SQL permettant de faire ces modifications dans un fichier migration.sql.
   ## Migration de la base.
   __Pour appliquer notre migration on doit:__
   1. Créer une nouvelle BDD sur le même modèle que la première (sans les données).
   2. Appliquer la migration (changer le modèle).
   3. Importer les données depuis la première base de données en appliquant les modifications nécessaires (par exemple remplacer le nom du pays par la PK de world.country).

> pour la langue des clients, on mettra la première (la plus courante) langue officielle du pays.

## Questions.
 __On veut :__
1. Pour chaque pays (avec au moins 1 client) le nombre de client, la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.
```sql
SELECT COUNT(classicmodelsv2.customers.customerName) AS "Nombre de clients", world.country.Name, classicmodelsv2.customers.language, classicmodelsv2.payments.amount
FROM classicmodelsv2.customers
JOIN classicmodelsv2.payments ON classicmodelsv2.payments.customerNumber = classicmodelsv2.customers.customerNumber
JOIN world.country ON country.Code = classicmodelsv2.customers.country
GROUP BY classicmodelsv2.customers.country;
```
| Nombre de clients | Name | language | amount |
| :--- | :--- | :--- | :--- |
| 16 | Australia | en | 45864.03 |
| 6 | Austria | de | 35826.33 |
| 6 | Belgium | nl | 45352.47 |
| 7 | Canada | en | 36527.61 |
| 2 | Switzerland | de | 47375.92 |
| 7 | Germany | de | 10549.01 |
| 6 | Denmark | da | 4710.73 |
| 23 | Spain | es | 36251.03 |
| 9 | Finland | fi | 23602.90 |
| 35 | France | fr | 6066.78 |
| 12 | United Kingdom | en | 52825.29 |
| 1 | Hong Kong | en | 45480.79 |
| 2 | Ireland | en | 17359.53 |
| 9 | Italy | it | 33924.24 |
| 6 | Japan | ja | 15183.63 |
| 8 | Norway | nb | 50218.95 |
| 11 | New Zealand | en | 75020.13 |
| 3 | Philippines | en | 20644.24 |
| 7 | Singapore | zh | 44380.15 |
| 4 | Sweden | sv | 36005.71 |
| 93 | United States | en | 14191.12 |

2. La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.
```sql
SELECT * FROM
(SELECT classicmodelsv2.customers.customerName, SUM(classicmodelsv2.payments.amount) AS CA
FROM classicmodelsv2.payments
LEFT JOIN classicmodelsv2.customers ON classicmodelsv2.payments.customerNumber = classicmodelsv2.customers.customerNumber
GROUP BY classicmodelsv2.customers.customerName ) as caList
ORDER BY CA DESC
LIMIT 10;
```
| customerName | CA |
| :--- | :--- |
| Euro+ Shopping Channel | 715738.98 |
| Mini Gifts Distributors Ltd. | 584188.24 |
| Australian Collectors, Co. | 180585.07 |
| Muscle Machine Inc | 177913.95 |
| Dragon Souveniers, Ltd. | 156251.03 |
| Down Under Souveniers, Inc | 154622.08 |
| AV Stores, Co. | 148410.09 |
| Anna's Decorations, Ltd | 137034.22 |
| Corporate Gift Ideas Co. | 132340.78 |
| Saveley & Henriot, Co. | 130305.35 |


3. La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande).
4. Les 10 produits les plus vendus.
```sql
SELECT classicmodelsv2.products.productName, SUM(classicmodelsv2.orderdetails.quantityOrdered) AS "Quantité"
FROM classicmodelsv2.orderdetails
JOIN classicmodelsv2.products ON classicmodelsv2.products.productCode = classicmodelsv2.orderdetails.productCode
GROUP BY classicmodelsv2.orderdetails.productCode
ORDER BY quantité DESC LIMIT 10;
```
| productName | Quantité |
| :--- | :--- |
| 1992 Ferrari 360 Spider red | 1808 |
| 1937 Lincoln Berline | 1111 |
| American Airlines: MD-11S | 1085 |
| 1941 Chevrolet Special Deluxe Cabriolet | 1076 |
| 1930 Buick Marquette Phaeton | 1074 |
| 1940s Ford truck | 1061 |
| 1969 Harley Davidson Ultimate Chopper | 1057 |
| 1957 Chevy Pickup | 1056 |
| 1964 Mercedes Tour Bus | 1053 |
| 1956 Porsche 356A Coupe | 1052 |

5. Pour chaque pays le produits le plus vendu.
6. Le produit qui a rapporté le plus de bénéfice.
7. La moyenne des différences entre le MSRP et le prix de vente (en pourcentage).
8. Pour chaque pays (avec au moins 1 client) le nombre de bureaux dans le même pays.