import mysql.connector as mysql
import pandas as pd
import plotly.express as px

conn = mysql.connect(host='localhost', user='root', password='pass', database='world')
cursor = conn.cursor(dictionary=True)
cursor.execute("SELECT Region, Name, Code FROM country;")

df = pd.DataFrame(cursor.fetchall(), columns=['Region', "Code", "Name"])

fig = px.choropleth(data_frame=df, locations="Code",
                    hover_name="Name", color="Region", range_color=(0, 12),
                    hover_data=["Region"],
                    color_continuous_scale=px.colors.sequential.Plasma)

fig.show()
