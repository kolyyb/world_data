# Analyse de données
## Le modèle physique des donées
![MPD](./MDP.png)

## Pour chaque type utilisé pour au moins un attribut : le definir et expliquer pourquoi on a choisit ce type ?
__Table country__

le champ  _continent_ est de type enum

Une énumération ENUM est une chaîne dont la valeur est choisie parmi une liste de valeurs autorisées lors de la création de la table.
Chaque country appartient à un continent (une table en moins)

## une "explication" ou "description" des informations que l'on peut tirer de chaque colonne de chaque table.

**Table city :**

une liste, pour chaque table, des contraintes (pourquoi il y a ces contraintes ?)

**city:**

* Tous les champs ont une contraite NOT NULL et ont une valeur imposée à une chaine de caractere vide (sauf pour population par defaut à 0) 
* le countryCode recupere le code de la table country 

**country**
* code : normalisation internationale ISO3166-1 de nomage des codes pays sur 3 caracteres
* https://fr.wikipedia.org/wiki/ISO_3166-1
* la reference code vers la table city.coutryCode
* la SurfaceArea est calculée en km2 2 chiffres apres la virgule
* LifeExpectancy : esperence de vie age en 
* IndepYear : année d'indépendance des pays en année
* GNPOld/GNP : Gross National Product mesure du PNB evolutif sur 1 an en million par pays
* LocalName : nom du pays en langue locale
* GovernmentForml : Regime politique du pays
* HeadOfState : Président / roi / reine du pays
* code2 meme normalisation ISO 3166-1 Alpha 2 code pays sur 2 caracteres
* 
**countryLanguage**
* isOfficial enum de langue officiel du pays (par code pays) et le pourcentage parlé

## Requetes
* __Liste des type de gouvernement avec nombre de pays pour chaque.__
``` sql
SELECT COUNT(name) AS 'nombre de pays', GovernmentForm AS 'Type de Gouvernement' FROM country GROUP BY GovernmentForm;
```

| nombre de pays | Type de Gouvernement |
| :--- | :--- |
| 1 | Administrated by the UN |
| 1 | Autonomous Area |
| 1 | Co-administrated |
| 2 | Commonwealth of the US |
| 29 | Constitutional Monarchy |
| 1 | Constitutional Monarchy \(Emirate\) |
| 4 | Constitutional Monarchy, Federation |
| 2 | Dependent Territory of Norway |
| 12 | Dependent Territory of the UK |
| 1 | Dependent Territory of the US |
| 1 | Emirate Federation |
| 14 | Federal Republic |
| 1 | Federation |
| 1 | Independent Church State |
| 1 | Islamic Emirate |
| 2 | Islamic Republic |
| 5 | Monarchy |
| 1 | Monarchy \(Emirate\) |
| 2 | Monarchy \(Sultanate\) |
| 4 | Nonmetropolitan Territory of France |
| 3 | Nonmetropolitan Territory of New Zealand |
| 2 | Nonmetropolitan Territory of The Netherlands |
| 1 | Occupied by Marocco |
| 4 | Overseas Department of France |
| 1 | Parlementary Monarchy |
| 1 | Parliamentary Coprincipality |
| 2 | Part of Denmark |
| 1 | People'sRepublic |
| 123 | Republic |
| 3 | Socialistic Republic |
| 1 | Socialistic State |
| 2 | Special Administrative Region of China |
| 2 | Territorial Collectivity of France |
| 4 | Territory of Australia |
| 3 | US Territory |

* __Pourquoi countrylanguage.IsOfficial utilise un enum et pas un bool ?__


* __D’apres la BDD, combien de personne dans le monde parle anglais ?__

```sql
SELECT SUM((Population * Percentage) /100) AS 'nombre de personnes parlant anglais dans le monde' FROM country
JOIN countrylanguage ON CountryCode =  country.Code WHERE countrylanguage.Language = 'English';
```
| nombre de personnes parlant anglais dans le monde |
| :--- |
| 347077867.30000 |

* __Faire la liste des langues avec le nombre de locuteur, de la plus parlée à la moins parlée.__

```sql
SELECT Language, SUM(Population * Percentage) /100  AS Locuteur FROM country
JOIN countryLanguage ON country.Code = countrylanguage.CountryCode
GROUP BY  Language ORDER BY Locuteur DESC;
```
| Language | Locuteur |
| :--- | :--- |
| Chinese | 1191843539.00000 |
| Hindi | 405633070.00000 |
| Spanish | 355029462.00000 |
| English | 347077867.30000 |
| Arabic | 233839238.70000 |
| Bengali | 209304719.00000 |
| Portuguese | 177595269.40000 |
| Russian | 160807561.30000 |
| Japanese | 126814108.00000 |
| Punjabi | 104025371.00000 |

* __En quelle unité est exprimée la surface des pays ?__
* ```sql
  SELECT Name, SurfaceArea FROM country WHERE Name = 'Benin';
  ```
  | Name | SurfaceArea |
| :--- | :--- |
| Benin | 112622.00 |

* __Faire la liste des pays qui ont plus 10 000 000 d’hab. avec leur capitale et le % de la population qui habite dans la capitale.__

```sql
SELECT country.Name, country.Population, city.Name, (city.Population / country.Population) * 100 AS '% d\'habitants par Capitale' FROM country
JOIN city ON city.ID = country.Capital
WHERE country.Population  > 10000000 ORDER BY country.Population ASC ;
```
| Name | Population | Name | % d'habitants par Capitale |
| :--- | :--- | :--- | :--- |
| Hungary | 10043200 | Budapest | 18.0376 |
| Somalia | 10097000 | Mogadishu | 9.8742 |
| Belarus | 10236000 | Minsk | 16.3540 |
| Belgium | 10239000 | Bruxelles \[Brussel\] | 1.3073 |
| Czech Republic | 10278100 | Praha | 11.4917 |
| Greece | 10545700 | Athenai | 7.3212 |
| Yugoslavia | 10640000 | Beograd | 11.3158 |
| Niger | 10730000 | Niamey | 3.9143 |
| Malawi | 10925000 | Lilongwe | 3.9905 |
| Cambodia | 11168000 | Phnom Penh | 5.1053 |
| Cuba | 11201000 | La Habana | 20.1411 |
| Mali | 11234000 | Bamako | 7.2063 |
| Guatemala | 11385000 | Ciudad de Guatemala | 7.2315 |
| Zimbabwe | 11669000 | Harare | 12.0833 |
| Burkina Faso | 11937000 | Ouagadougou | 6.9029 |
| Ecuador | 12646000 | Quito | 12.4423 |
| Angola | 12878000 | Luanda | 15.7012 |
| Côte d’Ivoire | 14786000 | Yamoussoukro | 0.8792 |
| Cameroon | 15085000 | Yaoundé | 9.1004 |
| Chile | 15211000 | Santiago de Chile | 30.9247 |
| Netherlands | 15864000 | Amsterdam | 4.6092 |
| Madagascar | 15942000 | Antananarivo | 4.2383 |
| Syria | 16125000 | Damascus | 8.3535 |
| Kazakstan | 16223000 | Astana | 1.9183 |
| Yemen | 18112000 | Sanaa | 2.7805 |
| Sri Lanka | 18827000 | Colombo | 3.4259 |
| Australia | 18886000 | Canberra | 1.7088 |
| Mozambique | 19680000 | Maputo | 5.1775 |
| Ghana | 20212000 | Accra | 5.2939 |
| Saudi Arabia | 21607000 | Riyadh | 15.3839 |
| Uganda | 21778000 | Kampala | 4.0904 |
| Malaysia | 22244000 | Kuala Lumpur | 5.8332 |
| Taiwan | 22256000 | Taipei | 11.8679 |
| Romania | 22455500 | Bucuresti | 8.9783 |
| Afghanistan | 22720000 | Kabul | 7.8345 |
| Iraq | 23115000 | Baghdad | 18.7584 |
| Nepal | 23930000 | Kathmandu | 2.4732 |
| North Korea | 24039000 | Pyongyang | 10.3332 |
| Venezuela | 24170000 | Caracas | 8.1725 |
| Uzbekistan | 24318000 | Toskent | 8.7075 |
| Peru | 25662000 | Lima | 25.1917 |
| Morocco | 28351000 | Rabat | 2.1991 |
| Sudan | 29490000 | Khartum | 3.2129 |
| Kenya | 30080000 | Nairobi | 7.6130 |
| Canada | 31147000 | Ottawa | 1.0764 |
| Algeria | 31471000 | Alger | 6.8889 |
| Tanzania | 33517000 | Dodoma | 0.5639 |
| Argentina | 37032000 | Buenos Aires | 8.0529 |
| Poland | 38653600 | Warszawa | 4.1791 |
| Spain | 39441700 | Madrid | 7.2995 |
| South Africa | 40377000 | Pretoria | 1.6312 |
| Colombia | 42321000 | Santafé de Bogotá | 14.7937 |
| Myanmar | 45611000 | Rangoon \(Yangon\) | 7.3704 |
| South Korea | 46844000 | Seoul | 21.3082 |
| Ukraine | 50456000 | Kyiv | 5.2006 |
| Congo, The Democratic Republic of the | 51654000 | Kinshasa | 9.8037 |
| Italy | 57680000 | Roma | 4.5832 |
| France | 59225700 | Paris | 3.5884 |
| United Kingdom | 59623400 | London | 12.2184 |
| Thailand | 61399000 | Bangkok | 10.2936 |
| Ethiopia | 62565000 | Addis Abeba | 3.9879 |
| Turkey | 66591000 | Ankara | 4.5624 |
| Iran | 67702000 | Teheran | 9.9832 |
| Egypt | 68470000 | Cairo | 9.9160 |
| Philippines | 75967000 | Manila | 2.0813 |
| Vietnam | 79832000 | Hanoi | 1.7662 |
| Germany | 82164700 | Berlin | 4.1218 |
| Mexico | 98881000 | Ciudad de México | 8.6885 |
| Nigeria | 111506000 | Abuja | 0.3140 |
| Japan | 126714000 | Tokyo | 6.2978 |
| Bangladesh | 129155000 | Dhaka | 2.7973 |
| Russian Federation | 146934000 | Moscow | 5.7095 |
| Pakistan | 156483000 | Islamabad | 0.3352 |
| Brazil | 170115000 | Brasília | 1.1580 |
| Indonesia | 212107000 | Jakarta | 4.5283 |
| United States | 278357000 | Washington | 0.2055 |
| India | 1013662000 | New Delhi | 0.0297 |
| China | 1277558000 | Peking | 0.5849 |


* __Liste des 10 pays avec le plus fort taux de croissance entre n et n-1 avec le % de croissance__
```sql
SELECT country.Name, country.GNP, country.GNPOld, ((country.GNP - country.GNPOld) / country.GNP *100) AS Croissance
FROM country
WHERE GNPOld IS NOT NULL
ORDER BY Croissance DESC
LIMIT 10;
```
| Name | GNP | GNPOld | Croissance |
| :--- | :--- | :--- | :--- |
| Congo, The Democratic Republic of the | 6964.00 | 2474.00 | 64.474440 |
| Turkmenistan | 4397.00 | 2000.00 | 54.514442 |
| Tajikistan | 1990.00 | 1056.00 | 46.934673 |
| Estonia | 5328.00 | 3371.00 | 36.730480 |
| Albania | 3205.00 | 2500.00 | 21.996880 |
| Suriname | 870.00 | 706.00 | 18.850575 |
| Iran | 195746.00 | 160151.00 | 18.184280 |
| Bulgaria | 12178.00 | 10169.00 | 16.496962 |
| Honduras | 5333.00 | 4697.00 | 11.925745 |
| Latvia | 6398.00 | 5639.00 | 11.863082 |


* __Liste des pays plurilingues avec pour chacun le nombre de langues parlées.__
```sql
SELECT country.Name, countrylanguage.Language,
COUNT(countrylanguage.Language) AS Nb_Languages
FROM country
JOIN countrylanguage ON countrylanguage.CountryCode = country.Code
GROUP BY country.Name
HAVING COUNT(countrylanguage.Language) > 1
LIMIT 12;
```
| Name | Language | Nb\_Languages |
| :--- | :--- | :--- |
| Afghanistan | Balochi | 5 |
| Albania | Albaniana | 3 |
| Algeria | Arabic | 2 |
| American Samoa | English | 3 |
| Andorra | Catalan | 4 |
| Angola | Ambo | 9 |
| Antigua and Barbuda | Creole English | 2 |
| Argentina | Indian Languages | 3 |
| Armenia | Armenian | 2 |
| Aruba | Dutch | 4 |
| Australia | Arabic | 8 |
| Austria | Czech | 8 |

* __Liste des pays avec plusieurs langues officielles, le nombre de langues officielle et le nombre de langues du pays.__

```sql
SELECT cc.Name, cc.Languages, cc.Offical_Languages
FROM (SELECT CountryCode,
             country.Name,
             Language,
             count(*) AS Languages,
             sum(case when IsOfficial = 'T' then 1 else 0 end) AS Offical_Languages,
             IsOfficial
      FROM country
               JOIN countrylanguage ON country.Code = countrylanguage.CountryCode
      GROUP BY Country.Name) AS CC
WHERE cc.IsOfficial = 'T';
```
| Name | Languages | Offical\_Languages |
| :--- | :--- | :--- |
| Albania | 3 | 1 |
| Algeria | 2 | 1 |
| American Samoa | 3 | 2 |
| Andorra | 4 | 1 |
| Anguilla | 1 | 1 |
| Armenia | 2 | 1 |
| Aruba | 4 | 1 |
| Bahrain | 2 | 1 |
| Bangladesh | 7 | 1 |
| Belarus | 4 | 2 |
| Belize | 4 | 1 |
| Bermuda | 1 | 1 |
| Bolivia | 4 | 3 |
| Bosnia and Herzegovina | 1 | 1 |
| Bulgaria | 4 | 1 |
| Burundi | 3 | 2 |
| Cayman Islands | 1 | 1 |
| Chad | 8 | 1 |
| China | 12 | 1 |
| Cocos \(Keeling\) Islands | 2 | 1 |
| Comoros | 5 | 1 |
| Croatia | 2 | 1 |
| Cuba | 1 | 1 |
| Cyprus | 2 | 2 |
| Czech Republic | 8 | 1 |
| East Timor | 2 | 1 |
| Egypt | 2 | 1 |
| Falkland Islands | 1 | 1 |
| Faroe Islands | 2 | 2 |
| Fiji Islands | 2 | 1 |
| Germany | 6 | 1 |
| Greece | 2 | 1 |
| Greenland | 2 | 2 |
| Guam | 5 | 2 |
| Haiti | 2 | 1 |
| Holy See \(Vatican City State\) | 1 | 1 |
| Iraq | 5 | 1 |
| Ireland | 2 | 2 |
| Israel | 3 | 2 |
| Jordan | 3 | 1 |
| Kiribati | 2 | 1 |
| Kuwait | 2 | 1 |
| Laos | 4 | 1 |
| Lebanon | 3 | 1 |
| Lesotho | 3 | 2 |
| Libyan Arab Jamahiriya | 2 | 1 |
| Liechtenstein | 3 | 1 |
| Luxembourg | 5 | 3 |
| Madagascar | 2 | 2 |
| Malawi | 4 | 1 |
| Maldives | 2 | 1 |
| Malta | 2 | 2 |
| Marshall Islands | 2 | 2 |
| Mayotte | 3 | 1 |
| Montserrat | 1 | 1 |
| Morocco | 2 | 1 |
| Myanmar | 8 | 1 |
| Netherlands Antilles | 3 | 2 |
| New Caledonia | 3 | 1 |
| New Zealand | 2 | 1 |
| Niue | 2 | 1 |
| Norfolk Island | 1 | 1 |
| Oman | 2 | 1 |
| Peru | 3 | 3 |
| Portugal | 1 | 1 |
| Qatar | 2 | 1 |
| Rwanda | 2 | 2 |
| Saint Helena | 1 | 1 |
| Saint Pierre and Miquelon | 1 | 1 |
| Samoa | 3 | 2 |
| San Marino | 1 | 1 |
| Saudi Arabia | 1 | 1 |
| Seychelles | 3 | 2 |
| Singapore | 3 | 3 |
| Somalia | 2 | 2 |
| South Africa | 11 | 4 |
| Sudan | 10 | 1 |
| Svalbard and Jan Mayen | 2 | 1 |
| Swaziland | 2 | 1 |
| Switzerland | 4 | 4 |
| Syria | 2 | 1 |
| Tokelau | 2 | 1 |
| Tonga | 2 | 2 |
| Tunisia | 3 | 1 |
| Turks and Caicos Islands | 1 | 1 |
| Tuvalu | 3 | 2 |
| United Arab Emirates | 2 | 1 |
| United Kingdom | 3 | 1 |
| United States Minor Outlying Islands | 1 | 1 |
| Uruguay | 1 | 1 |
| Vanuatu | 3 | 3 |
| Virgin Islands, British | 1 | 1 |
| Virgin Islands, U.S. | 3 | 1 |
| Western Sahara | 1 | 1 |
| Yemen | 2 | 1 |
| Zimbabwe | 4 | 1 |

* __Liste des langues parlées en France, Chine, Etats Unis et Royaumes Unis avec le % pour chacune.__
``` sql
SELECT Language, countrylanguage.Percentage  FROM countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode WHERE country.Name = 'France';
```
| Language | Percentage |
| :--- | :--- |
| Arabic | 2.5 |
| French | 93.6 |
| Italian | 0.4 |
| Portuguese | 1.2 |
| Spanish | 0.4 |
| Turkish | 0.4 |

* Pareil en chine.
```sql
SELECT Language, countrylanguage.Percentage  FROM countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode WHERE country.Name = 'China';
```
| Language | Percentage |
| :--- | :--- |
| Chinese | 92.0 |
| Dong | 0.2 |
| Hui | 0.8 |
| Mantšu | 0.9 |
| Miao | 0.7 |
| Mongolian | 0.4 |
| Puyi | 0.2 |
| Tibetan | 0.4 |
| Tujia | 0.5 |
| Uighur | 0.6 |
| Yi | 0.6 |
| Zhuang | 1.4 |

* Pareil aux états unis.
```sql
SELECT Language, countrylanguage.Percentage  FROM countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode WHERE country.Name = 'United States';
```
| Language | Percentage |
| :--- | :--- |
| Chinese | 0.6 |
| English | 86.2 |
| French | 0.7 |
| German | 0.7 |
| Italian | 0.6 |
| Japanese | 0.2 |
| Korean | 0.3 |
| Polish | 0.3 |
| Portuguese | 0.2 |
| Spanish | 7.5 |
| Tagalog | 0.4 |
| Vietnamese | 0.2 |

* Pareil aux UK.
```sql
SELECT Language, countrylanguage.Percentage  FROM countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode WHERE country.Name = 'United Kingdom';
```

| Language | Percentage |
| :--- | :--- |
| English | 97.3 |
| Gaeli | 0.1 |
| Kymri | 0.9 |

* Pour chaque région quelle est la langue la plus parler et quel pourcentage de la population la parle
```sql
select Name, Language, Percentage
from country
         join (SELECT cc.CountryCode, Language, Percentage
               from countrylanguage
                        join(select DISTINCT CountryCode, max(Percentage) as MAx
                             from countrylanguage
                             group by CountryCode) as cc
                            on countrylanguage.CountryCode = cc.CountryCode and
                               cc.MAx = countrylanguage.Percentage) as CC1
              on country.Code = CC1.CountryCode LIMIT 12;
```
| Name | Language | Percentage |
| :--- | :--- | :--- |
| Aruba | Papiamento | 76.7 |
| Afghanistan | Pashto | 52.4 |
| Angola | Ovimbundu | 37.2 |
| Anguilla | English | 0.0 |
| Albania | Albaniana | 97.9 |
| Andorra | Spanish | 44.6 |
| Netherlands Antilles | Papiamento | 86.2 |
| United Arab Emirates | Arabic | 42.0 |
| Argentina | Spanish | 96.8 |
| Armenia | Armenian | 93.4 |
| American Samoa | Samoan | 90.6 |
| Antigua and Barbuda | Creole English | 95.7 |


