import csv
import mysql.connector as mysql

conn = mysql.connect(host='localhost', user='root', password='pass', database='classicmodelsv2')
cur = conn.cursor(dictionary=True)

# Recuperer le code iso à partir du Langauge
def get_language_code(Language):
    # Charger le csv iso
    csv_file = csv.reader(open('iso_639-1.csv', 'r'), delimiter=",")
    for row in csv_file:
        if Language == 'Pilipino':
            return 'en'
        elif Language in row[1]:
            return row[3]


# Update du champ language de la table customers
cur.execute(
    """SELECT classicmodelsv2.customers.customerNumber, classicmodelsv2.customers.country FROM classicmodelsv2.customers""")
customers_list = cur.fetchall()
for customer in customers_list:
    cur.execute("""SELECT world.countryLanguage.Language FROM world.countryLanguage
    WHERE IsOfficial = 'T' AND countrylanguage.CountryCode = %s ORDER BY Percentage DESC LIMIT 1;""",
                [customer['country']])
    official_language = cur.fetchone()
    cur.execute("""UPDATE classicmodelsv2.customers SET classicmodelsv2.customers.Language = %s 
    WHERE classicmodelsv2.customers.customerNumber = %s;""",[get_language_code(official_language['Language']), customer['customerNumber']])
    conn.commit()
    cur.close()
    conn.close()

